// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

import "./IOwnable.sol";

/**
 * @title Represents an ownable resource.
 */
contract Ownable is IOwnable {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * Constructor
     * @param addr The owner of the smart contract
     */
    constructor (address addr) {
        require(addr != address(0), "The address of the owner is required");
        _owner = addr;
        emit OwnershipTransferred(address(0), addr);
    }

    /**
     * @notice This modifier indicates that the function can only be called by the owner.
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(_owner == msg.sender, "Only the owner of the smart contract is allowed to call this function.");
        _;
    }

    /**
     * @notice Transfers ownership of the contract
     * @param addr Specifies the address
     */
    function transferOwnership (address addr) public onlyOwner {
        require(addr != address(0), "Ownable: new owner cannot be 0x0.");
        require(addr != address(1), "Ownable: new owner cannot be 0x1.");
        emit OwnershipTransferred(_owner, addr);
        _owner = addr;
    }

    /**
     * @notice Sets the owner to 0x0 address
     */
    function renounceOwnership() public virtual onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @notice Gets the address of the owner.
     * @return the address of the owner.
     */
    function owner() public override view returns (address) {
        return _owner;
    }
}