# README 

Digifox Token

### Contains DGF Token Smart Contracts 

- DigifoxToken
- Vesting
- (more including Minting, Airdrop, and testing contracts coming soon)

### DigifoxToken 

###### Contains:
- Basic token functionality
- Distribution Addresses
- Communication with other Smart Contracts
- Triggers Vesting

### Vesting 

###### Contains:
- Vesting functionality
- Withdrawal functionality
- Vesting is triggered from the DigifoxToken contract

### Airdrop 

###### Contains:
- Basic Uniswap-like Airdrop Functionality
- NOT FOR MASS-AIRDROPPING! Use MultiSender for traditional airdropping

### Minting 

###### Contains:
- Minting logic for Early Adopters Program
- Redeem logic for the Early Adopters Program
- Interactability with the Digifox Relayer contract (update required)
- Needs refactoring for Efficiency, decimal precision, and polymorphic minting (such as the liquidity and bonus programs)