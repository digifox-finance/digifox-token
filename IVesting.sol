// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

/**
 * @title Defines the interface for vesting
 */
interface IVesting {
    /**
    * @notice Starts vesting tokens.
    * @dev Used to vest tokens for a set period of time.
    * @param amount The amount of tokens to vest. Only callable from the DGF token contract.
    * @param blocksUntilCompleted The number of blocks the tokens should be vested for.
    */
    function vest(uint256 amount, uint256 blocksUntilCompleted) external;

    /**
    * @notice Withdraws funds to the owner address.
    * @dev Only callable by the owner.
    */
    function withdraw() external returns (uint256);
}