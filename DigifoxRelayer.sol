// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

import "./Ownable.sol";
import "./IDigifoxRelayer.sol";

// ADD THIS LOGIC TO THE RELAYER CONTRACT!
contract DigifoxRelayer is IDigifoxRelayer, Ownable {
    // A POINTS & FEES VAR SHOULD BE UPDATED WHENEVER DEPOSITS/SWAPS OCCUR BASED ON STAKING PERIOD 
    // TODO: IMPORTANT!! STOP ACCUMULATING REWARDS ONCE THE MINTING PERIOD HAS ENDED!!!!!!
    // TODO: MAKE POINT PRECISION TO BE 10^18
    struct RelayerAddresses { 
       mapping(uint256 => uint256) feesGeneratedPerPeriod;   // ie: Period 1 == 3020, Period 2 == 5020...
       mapping(uint256 => uint256) pointsGeneratedPerPeriod; // ie: Period 1 == 3220, Period 2 == 5720... MAKE POINT PRECISION TO BE 10^18
       uint256 stakingOrigin; // ie, started staking on block 19203...
       uint256 claimedRewards; // How much HAVE been claimed
       uint256 lastBlockRedeemed; // The last block that a redeem action occured (for staking)
       uint256 lastPeriodRedeemed; // The last period withdrawn from (for minting redeem logic)
       
       address owningAddress; // for convenience
    }
    
    mapping(address => RelayerAddresses) internal relayerAddresses;
    mapping(uint256 => uint256) internal periodsAndFees; // ie: Period 1 == $50,000, Period 2 == $75,000
    mapping(uint256 => uint256) internal periodsAndPoints; // ie: Period 1 == 54000 POINTS, Period 2 == 79000 POINTS
    //mapping(uint256 => uint256) internal periodsAndBlocks; // ie: Period 1 == Block 234213, Period 2 == Block 253422
    mapping(uint256 => uint256) internal periodsAndActualMinted; // ie: Period 1 == 80,000 DGF, Period 2 == 52,352 DGF
    mapping(address => bool) internal addressIsEligable;
    uint256 public currentPeriod;
    address private _minter;
    
    // The number of blocks in a month (195810 blocks every 30 days)
    uint256 internal constant blocksInAMonth = 195810;
    
    constructor() Ownable(msg.sender) {
        currentPeriod = 1;
        _minter = address(0x0);
    }
    
    modifier onlyMinter() {
        require(msg.sender == _minter, "Only the minter may use this function.");
        _;
    }
    
    // General Upkeep Functions
    function setMinter(address addr) public onlyOwner {
        require(addr != address(0), "The address cannot be the zero address");
        _minter = addr;
    }

    function setCurrentPeriod(uint256 _period) public override onlyMinter {
        require(_period > 0, "The period must be greater than zero");
        currentPeriod = _period;
    }
    
    function setPeriodMintedTokens(uint256 _period, uint256 _minted) public override onlyMinter {
        require(_period > 0, "The period must be greater than zero");
        periodsAndActualMinted[_period] = _minted;
    }
    
    function addToClaimedRewards(address _redeemer, uint256 _minted) public override onlyMinter {
        require(_redeemer != address(0), "The address cannot be the zero address");
        relayerAddresses[_redeemer].claimedRewards = relayerAddresses[_redeemer].claimedRewards + (_minted);
    }
    
    function updateLastTimeRedeemed(address _redeemer, uint256 _period) public override onlyMinter {
        require(_redeemer != address(0), "The address cannot be the zero address");
        relayerAddresses[_redeemer].lastPeriodRedeemed = _period;
        relayerAddresses[_redeemer].lastBlockRedeemed = block.number;
    }
    
    function feeGeneratingActionTriggered(address _user, uint256 _fees) public onlyOwner {
        require(_user != address(0), "The address cannot be the zero address");

        uint256 pointsGenerated;
        
        if (addressIsEligable[_user]) {
            relayerAddresses[_user].feesGeneratedPerPeriod[currentPeriod] = relayerAddresses[_user].feesGeneratedPerPeriod[currentPeriod] + _fees;
            periodsAndFees[currentPeriod] = periodsAndFees[currentPeriod] + _fees;
            
            pointsGenerated = relayerAddresses[_user].pointsGeneratedPerPeriod[currentPeriod] + (determineCurrentStakingMultiplier(_user) * _fees);
            
            relayerAddresses[_user].pointsGeneratedPerPeriod[currentPeriod] = relayerAddresses[_user].pointsGeneratedPerPeriod[currentPeriod] + pointsGenerated;
            periodsAndPoints[currentPeriod] = periodsAndPoints[currentPeriod] + pointsGenerated;
        }
        else {
            addressIsEligable[_user] = true;
            relayerAddresses[_user].owningAddress = _user;
            relayerAddresses[_user].stakingOrigin = block.number;
            
            relayerAddresses[_user].feesGeneratedPerPeriod[currentPeriod] = _fees;
            periodsAndFees[currentPeriod] = _fees;
            
            relayerAddresses[_user].pointsGeneratedPerPeriod[currentPeriod] = _fees;
            periodsAndPoints[currentPeriod] = _fees;
        }
    }
    
    // Views
    
    function getTotalFeesOfPeriod(uint256 _period) public override view returns (uint256 totalReward) {
        return periodsAndFees[_period];
    }
    
    function getPointsOfPeriod(uint256 _period) public override view returns (uint256 totalPoints) {
        return periodsAndPoints[_period];
    }
    
    function checkAccumulatedRewards(address _check) public override view returns (uint256 accumulatedRewards) {
        require(_check != address(0), "The address cannot be the zero address");
        uint256 lastRedeemed = relayerAddresses[_check].lastPeriodRedeemed;

        if (lastRedeemed == currentPeriod) return 0;

        for(uint256 i = lastRedeemed + 1; i <= currentPeriod; i++){
            accumulatedRewards = accumulatedRewards + (relayerAddresses[_check].pointsGeneratedPerPeriod[i] / getPointsOfPeriod(i));
        }
        
        return accumulatedRewards;
    }
    
    
    function getPointsOfUserByPeriod(address _redeemer, uint256 _period) public override view onlyMinter returns (uint256 points) {
        require(_redeemer != address(0), "The address cannot be the zero address");
        return relayerAddresses[_redeemer].pointsGeneratedPerPeriod[_period];
    }
    
    function checkLastPeriodRedeemed(address _check) public override view onlyMinter returns (uint256 lastPeriod) {
        require(_check != address(0), "The address cannot be the zero address");
        return relayerAddresses[_check].lastPeriodRedeemed;
    }
    
    /*  THIS WILL NEED TO BE CALLED WHENEVER A FEE-GENERATING ACTION TAKES
    /*  PLACE AND UPDATE THE pointsGeneratedPerPeriod VARIABLE IN relayerAddresses.
    /***********************************************************************
    
    /**
     * @notice Used to determine extra rewards for users who are staking.
     * @param _check the address to check the staking reward multiplier of.
     * @return stakingMultiplier the current yield from staking.
     */
    function determineCurrentStakingMultiplier(address _check) private view returns (uint256 stakingMultiplier) {
        require(_check != address(0), "The address cannot be the zero address");

        uint256 blocksStaked = block.number - relayerAddresses[_check].lastBlockRedeemed;
        uint256 blocksInAYear = blocksInAMonth * 12;

        // max multiplier must be 2x AFTER 2 YEARS
        stakingMultiplier = ((blocksStaked * 1000) / blocksInAYear) + 500;

        // 2x is max
        return (stakingMultiplier > 2000) ? 2000 : stakingMultiplier;
    }
    
  
}