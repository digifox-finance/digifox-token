// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

import "./IERC20.sol";
import "./IVesting.sol";
import "./Ownable.sol";

/**
 * @title Contract with simple vesting functions.
 * @dev Sends tokens to the contract owner/deployer ONLY.
 */
contract Vesting is IVesting, Ownable {
    uint256 public _depositedAmount;
    uint256 public _withdrawnAmount;
    uint256 public _finalBlock;
    uint256 public _vestedPeriod;
    IERC20 private _token;
    bool public _hasVested;
    
    /**
     * @notice Constructor
     */
    constructor() Ownable(msg.sender) {
        _hasVested = false;
        _withdrawnAmount = 0;
    }

    modifier onlyTokenContract() {
        require(msg.sender == address(_token));
        _;
    }
    
    modifier onlyIfVestedPeriodStarted() {
        require(_hasVested);
        _;
    }

    /**
    * @notice Sets the address of the token.
    * @dev Only callable by the owner
    * @param tokenAddr The address of the token
    */
    function setDGFAddress(address tokenAddr) public onlyOwner {
        require(tokenAddr != address(0), "Token address cannot be the zero address");
        _token = IERC20(tokenAddr);
    }

    /**
    * @notice Starts vesting tokens.
    * @dev Used to vest tokens for a set period of time.
    * @param amount The amount of tokens to vest. Only callable from the DGF token contract.
    * @param blocksUntilCompleted The number of blocks the tokens should be vested for.
    */
    function vest(uint256 amount, uint256 blocksUntilCompleted) public override onlyTokenContract { 
        require(amount > 0, "The amount must be greater than zero");
        require(blocksUntilCompleted > 0, "The number of blocks must be greater than zero");
        require(!_hasVested, "Vesting already started");

        _hasVested = true;
        _depositedAmount = amount;
        _withdrawnAmount = 0;
        _vestedPeriod = blocksUntilCompleted;
        _finalBlock = block.number + blocksUntilCompleted;
    }

    /**
    * @notice Withdraws funds to the owner address.
    * @dev Only callable by the owner.
    */
    function withdraw() public override onlyIfVestedPeriodStarted onlyOwner returns (uint256) {
        require(address(_token) != address(0), "The address of the token was not set");

        if (block.number > _finalBlock) {
            uint256 amountToWithdraw = _token.balanceOf(address(this));
            _token.transfer(owner(), amountToWithdraw);
            _hasVested = false;
            return amountToWithdraw;
        } 
        else {
            // The number of blocks to compute
            uint256 numberOfBlocks = getNumberOfBlocks();
            require(numberOfBlocks > 0, "The number of blocks must be greater than zero");

            uint256 numerator = _depositedAmount * numberOfBlocks;
            require(numerator > 0, "The numerator must be greater than zero");

            uint256 allowedAmount = numerator / _vestedPeriod;
            require(allowedAmount > 0, "The allowed amount must be greater than zero");

            uint256 amountToWithdraw = allowedAmount - _withdrawnAmount;
            require(amountToWithdraw > 0, "The amount to withdraw must be greater than zero");

            // Run the ERC20 transfer
            require(_token.transfer(owner(), amountToWithdraw), "ERC20 transfer failed");
            
            // State changes
            _withdrawnAmount = _withdrawnAmount + amountToWithdraw;

            return amountToWithdraw;
        }
    }

    /**
    * @notice Gets the number of blocks that can be withdrawn
    * @return Returns the number of blocks
    */
    function getNumberOfBlocks() public view returns (uint256) {
        return (block.number > _finalBlock) ? _finalBlock : block.number - (_finalBlock - _vestedPeriod);
    }
}