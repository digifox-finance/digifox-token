// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

/**
 * @title Defines the interface of an ownable resource
 */
interface IOwnable {
    /**
     * @notice Gets the address of the owner.
     * @return the address of the owner.
     */
    function owner() external view returns (address);
}
