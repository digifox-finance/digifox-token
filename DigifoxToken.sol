// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

import "./IVesting.sol";
import "./Ownable.sol";
import "./ERC20.sol";

/**
 * @title Represents an utility token. This utility token is an ERC-20 by definition.
 * @dev Notice that the token supply is fixed in our case. 
 *      Therefore minting and/or burning capabilities are not available. There is nothing to mint or burn in this case.
 */
contract DigifoxToken is Ownable, ERC20 {
    // ERC-20 private constants (pass them to the base ERC20 contract)
    string private constant _tokenName = "Digifox Token";
    string private constant _tokenSymbol = "DGF";
    uint8 private constant _tokenDecimals = 18;

    // This specific utility token has a fixed supply, as defined below. No minting or burning allowed.
    uint256 public constant fixedTotalSupply = 10000000 * 10**_tokenDecimals;

    // The number of blocks to vest is a constant also
    uint256 public constant blocksToVest = 9553030;

    // Vesting state
    bool private _vestingStarted;

    // Distribution wallets, smart contracts for fair distribution of DGF
    address private earlyAdopterDistributionWallet;         // MINTABLE (use Minting.sol)
    address private liquidityProviderDistributionWallet;    // MINTABLE (use Minting.sol)
    address private bonusDistributionWallet;                // MAYBE DON'T MAKE MINTABLE (use Minting.sol) Affiliate Program (track affiliate wallets & get a % of the )
    address private communityDistributionWallet;            // Vested for 4 years (9553030 blocks, use Vesting.sol)
    address private educationDistributionWallet;            // Vested for 4 years (9553030 blocks, use Vesting.sol)
    address private wefunderDistributionWallet;             // NOT VESTED! Directly airdrop to a list of wallets using MultiSender
    address private earlyInvestorsDistributionWallet;       // NOT VESTED! Uniswap-style airdrop (use Airdrop.sol)
    address private partnersDistributionWallet;             // NOT VESTED 
    address private ambassadorsDistributionWallet;          // NOT VESTED
    address private employeeDistributionWallet;             // NOT VESTED
    address private advisorDistributionWallet;              // NOT VESTED
    address private reserveWallet;                          // Vested for 4 years (9553030 blocks, use Vesting.sol), 1% available upon launch

    // Distribution amounts per wallet
    uint256 private earlyAdopterDistributionAmount                  = fixedTotalSupply / (4);
    uint256 private liquidityProviderDistributionAmount             = fixedTotalSupply / (20);
    uint256 private bonusDistributionAmount                         = fixedTotalSupply / (20);
    uint256 private communityDistributionAmount                     = fixedTotalSupply / (50);
    uint256 private educationDistributionAmount                     = fixedTotalSupply / (100);
    uint256 private wefunderDistributionAmount                      = fixedTotalSupply / (100);
    uint256 private earlyInvestorsDistributionAmount                = fixedTotalSupply / (100);
    uint256 private partnersDistributionAmount                      = (fixedTotalSupply / 50) * (9);
    uint256 private ambassadorsDistributionAmount                   = fixedTotalSupply / (50);
    uint256 private employeeDistributionAmount                      = (fixedTotalSupply / 50) * (9);
    uint256 private advisorDistributionAmount                       = fixedTotalSupply / (50);
    uint256 private reserveWalletAmountInitialUnlocked              = (fixedTotalSupply / 5) / (100);
    uint256 private reserveWalletAmount                             = (fixedTotalSupply / 5) - (reserveWalletAmountInitialUnlocked);

    /**
     * @notice Constructor
     * @param addresses The wallet addresses pre-allocated to this token
     */
    constructor(address[] memory addresses) 
    Ownable(msg.sender) 
    ERC20(_tokenName, _tokenSymbol, _tokenDecimals, fixedTotalSupply, fixedTotalSupply) {
        earlyAdopterDistributionWallet         = address(addresses[0]); // MINTABLE (use Minting.sol)
        liquidityProviderDistributionWallet    = address(addresses[1]); // MINTABLE (use Minting.sol)
        bonusDistributionWallet                = address(addresses[2]); // MAYBE DON'T MAKE MINTABLE (use Minting.sol) Affiliate Program (track affiliate wallets & get a % of the )
        communityDistributionWallet            = address(addresses[3]); // Vested for 4 years (9553030 blocks, use Vesting.sol)
        educationDistributionWallet            = address(addresses[4]); // Vested for 4 years (9553030 blocks, use Vesting.sol)
        wefunderDistributionWallet             = address(addresses[5]); // NOT VESTED! Directly airdrop to a list of wallets using MultiSender
        earlyInvestorsDistributionWallet       = address(addresses[6]); // NOT VESTED! Uniswap-style airdrop (use Airdrop.sol)
        partnersDistributionWallet             = address(addresses[7]); // NOT VESTED 
        ambassadorsDistributionWallet          = address(addresses[8]); // NOT VESTED
        employeeDistributionWallet             = address(addresses[9]); // NOT VESTED
        advisorDistributionWallet              = address(addresses[10]); // NOT VESTED
        reserveWallet                          = address(addresses[11]); // Vested for 4 years (9553030 blocks, use Vesting.sol), 1% available upon launch

        _vestingStarted = false;

        // 25% goes toward early adopter program
        transfer(earlyAdopterDistributionWallet, earlyAdopterDistributionAmount);

        // 5% goes toward liquidity provider program
        transfer(liquidityProviderDistributionWallet, liquidityProviderDistributionAmount);
        
        // 5% goes toward referral bonus program
        transfer(bonusDistributionWallet, bonusDistributionAmount);
        
        // 2% goes toward community development fund
        transfer(communityDistributionWallet, communityDistributionAmount);
        
        // 1% goes toward education grant program
        transfer(educationDistributionWallet, educationDistributionAmount);
        
        // 1% goes toward the wefunder rewards program
        transfer(wefunderDistributionWallet, wefunderDistributionAmount);
        
        // 1% goes toward the early investor rewards program
        transfer(earlyInvestorsDistributionWallet, earlyInvestorsDistributionAmount);
        
        // 18% goes toward strategic partnerships
        transfer(partnersDistributionWallet, partnersDistributionAmount);
        
        // 2% goes toward ambassadors
        transfer(ambassadorsDistributionWallet, ambassadorsDistributionAmount);
        
        // 18% goes toward the employee fund
        transfer(employeeDistributionWallet, employeeDistributionAmount);
        
        // 2% goes toward advisors
        transfer(advisorDistributionWallet, advisorDistributionAmount);

        // 20% goes to a company reserve, 1% unlocked immediately. 
        transfer(reserveWallet, reserveWalletAmount);

        // 1% of the 20% above gets unlocked immediately.
        transfer(IOwnable(reserveWallet).owner(), reserveWalletAmountInitialUnlocked);

        // Final Check: there no pending tokens to distribute (the final amount is zero)
        require(balanceOf(msg.sender) == 0, "Token: Invalid distribution rate");
    }

    /**
     * @notice Begins the vesting process of certain wallets once tokens have been transferred to distribution wallets.
     */
    function startVesting() public onlyOwner {
        // Checks
        require(!_vestingStarted, "Vesting started already");

        // Start vesting
        IVesting(communityDistributionWallet).vest(communityDistributionAmount, blocksToVest);
        IVesting(educationDistributionWallet).vest(educationDistributionAmount, blocksToVest);
        IVesting(reserveWallet).vest(reserveWalletAmount, blocksToVest);

        // State changes
        _vestingStarted = true;
    }
}