// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

import "./IDigifoxRelayer.sol";
import "./IERC20.sol";
import "./Ownable.sol";

/**
 * @title Contract for minting new tokens.
 */
contract Minting is Ownable {    
    IDigifoxRelayer private _relayer;
    IERC20 private _token;
    
    uint256 private _totalActualMinted;
    uint256 private _totalExtraTokens;
    uint256 private _periodsElapsed;
    uint256 private _lastBlockMinted;
    
    uint256 internal constant blocksInAMonth     = 195810; // 195810 blocks every 30 days
    uint256 private constant _decayMinuend       = 104000000000000000000000;
    uint256 private constant _decaySubtrahend    = 2122000000000000000000;
    uint256 private constant _decimals           = 18; 
    uint256 private constant _maxPeriods         = 49;
    
    mapping(uint256 => uint256) public actualMintedAmount;
    mapping(address => bool) public mintingAdmins;
    
    /**
     * @notice Constructor
     */
    constructor() Ownable(msg.sender) {
        _periodsElapsed = 1;
    }
    
    modifier monthPassedAndMintingActive() {
        // The last block minted must have been a month ago.
        require(block.number >= (_lastBlockMinted + blocksInAMonth) && _periodsElapsed <= _maxPeriods);
        _;
    }
    
    modifier onlyOwnerOrAdmin() {
        require(msg.sender == owner() || mintingAdmins[msg.sender], "Only the owner or admins may use this function.");
        _;
    }
    
    /**
    * @notice Sets the address of the token.
    * @dev Only callable by the owner or Admin
    * @param tokenAddr The address of the token
    */
    function setDGFAddress(address tokenAddr) public onlyOwnerOrAdmin {
        require(tokenAddr != address(0), "The token address cannot be the zero address");
        _token = IERC20(tokenAddr);
    }

    /**
    * @notice Sets the address of the relayer.
    * @dev Only callable by the owner or Admin
    * @param relayerAddr The address of the relayer
    */
    function setRelayerAddress(address relayerAddr) public onlyOwnerOrAdmin {
        require(relayerAddr != address(0), "The relayer address cannot be the zero address");
        _relayer = IDigifoxRelayer(relayerAddr);
    }
    
    /**
    * @notice Used to redeem the airdrop for another user.
    * @dev Only admins or the owner may use this function.
    * @param otherAddr The user you are redeeming for
    */
    function redeemAirdropForAddress(address otherAddr) private {
        require(address(_relayer) != address(0), "Minting: The address of the relayer cannot be the zero address");

        uint256 accumulatedRewards = _relayer.checkAccumulatedRewards(otherAddr);
        require(accumulatedRewards > 0, "There are no tokens left to redeem.");
        
        uint256 reward = checkRewardOf(otherAddr);
        require(reward > 0, "There is no reward. The current reward must be greater than zero");

        _relayer.addToClaimedRewards(otherAddr, reward);
        _token.transfer(otherAddr, reward);
        _relayer.updateLastTimeRedeemed(otherAddr, _periodsElapsed);
    }

    /**
    * @notice Used to redeem your airdop.
    */    
    function redeemAirdrop() public {
        redeemAirdropForAddress(msg.sender);
    }

    /**
    * @notice Sends the current balance to the address specified.
    * @dev Only callable by the owner or Admin
    * @param returnTo The address of the target recipient
    */
    function recoverFunds(address returnTo) public onlyOwnerOrAdmin {
        require(returnTo != address(0), "Minting: The address cannot be the zero address");
        _token.transfer(returnTo, _token.balanceOf(address(this)));
    }
        
    /**
    * @notice Used to mint tokens for early adopters.
    * @param dollarValue the current price DGF is valued at in THOUSANDS (1001 = $1.001).
    * @return Returns the number of tokens minted during this period
    */
    function mint(uint256 dollarValue) public onlyOwnerOrAdmin monthPassedAndMintingActive returns (uint256) {
        require(dollarValue > 0, "The value must be greater than zero");

        uint256 mintedDuringThisPeriod = 0;
        uint256 rewardMultiplier = determineCurrentRewardMultiplier(dollarValue, _relayer.getTotalFeesOfPeriod(_periodsElapsed));
        
        _lastBlockMinted = block.number;
        mintedDuringThisPeriod = (currentUpperBound() * rewardMultiplier) / 100000;
        
        _relayer.setPeriodMintedTokens(_periodsElapsed, mintedDuringThisPeriod);
        
        // Variables to maintain minting functionality within this contract
        _totalActualMinted = _totalActualMinted + mintedDuringThisPeriod;
        actualMintedAmount[_periodsElapsed] = mintedDuringThisPeriod;
        _periodsElapsed++;
        _totalExtraTokens = _totalExtraTokens + (currentUpperBound() - _totalActualMinted);
        
        _relayer.setCurrentPeriod(_periodsElapsed);

        return mintedDuringThisPeriod;
    }
    
    /**
    * @notice Used by the owner to add an admin for easy minting functionality maintenance.
    * @param adminAddr The address of the admin
    */
    function addAdmin(address adminAddr) public onlyOwner {
        require(adminAddr != address(0), "The address cannot be the zero address");
        require(!mintingAdmins[adminAddr], "The address specified was added already");

        mintingAdmins[adminAddr] = true;
    }
    
    /**
    * @notice Used by the owner to remove an admin for easy minting functionality maintenance.
    * @param adminAddr The address of the admin to remove
    */
    function revokeAdmin(address adminAddr) public onlyOwner {
        require(adminAddr != address(0), "The address cannot be the zero address");
        require(mintingAdmins[adminAddr], "The address specified is not an Admin, or its Admin access was revoked already");

        mintingAdmins[adminAddr] = false;
    }
    
    /**
    * @notice Used to redeem the airdrop for another user.
    * @dev Only admins or the owner may use this function.
    * @param otherAddr The user you are redeeming for
    */
    function redeemAirdropForAnother(address otherAddr) public onlyOwnerOrAdmin {
        redeemAirdropForAddress(otherAddr);
    }

    /**
     * @notice Used to get the percent of the upper bound to mint. 
     * @dev Percent that is generated is in the HUNDRED THOUSANDS for precision purposes (ie: 24556 = 24.556%).
     * @param dollarValue the current price DGF is valued at in THOUSANDS (1001 = $1.001).
     * @param totalFeesGenerated the current dollar value in fees generated since the last minting (no decimal).
     * @return the current percent to mint.
     */
    function determineCurrentRewardMultiplier(uint256 dollarValue, uint256 totalFeesGenerated) public view returns (uint256) {
        uint256 currMultiplier = (totalFeesGenerated * 100000) / (dollarValue * (currentUpperBound() / (10 ** _decimals)) / (1000));
        
        return (currMultiplier < 100001) ? currMultiplier : 100000;
    }
    
    /**
     * @notice Used to determine if the calling address has a reward.
     * @return the reward
     */
    function checkReward() public view returns (uint256) {
        return checkRewardForAddress(msg.sender);
    }

    /**
     * @notice Used to determine if the address specified has a reward.
     * @param addr Specifies the address
     * @return the reward
     */
    function checkRewardOf(address addr) public onlyOwnerOrAdmin view returns (uint256) {
        return checkRewardForAddress(addr);
    }

    /**
     * @notice Calculates the reward of the address specified.
     * @param addr Specifies the public address of the user.
     * @return Returns the reward applicable to the address specified.
     */
    function checkRewardForAddress(address addr) internal view returns (uint256) {
        // The number of periods elapsed get incremented by one during minting only. That's the single entry point.
        // The maximum number of periods is constant. The value currently defined for it is "49".
        // This check ensures the loop below has a clearly defined boundary.
        require(_periodsElapsed <= _maxPeriods, "The number of periods elapsed is invalid");

        uint256 totalReward = 0;

        // Define the lower boundary. The maximum upper boundary is defined by "_maxPeriods"
        uint256 lastPeriodRedeemed = _relayer.checkLastPeriodRedeemed(addr);
        uint256 startPoint = (lastPeriodRedeemed > 0) ? lastPeriodRedeemed : 1;

        // Compute the reward at each data point in the search space.
        for (uint256 i = startPoint; i <= _periodsElapsed; i++) {
            totalReward += (((_relayer.getPointsOfUserByPeriod(addr, i)) * (10**_decimals)) / (_relayer.getPointsOfPeriod(i)));
        }

        return totalReward;
    }
    
    /**
    * @notice Used to calculate the current upper bound reward.
    * @return Returns the current upper bound
    */    
    function currentUpperBound() public view returns (uint256) {
        return _decayMinuend - (_decaySubtrahend * _periodsElapsed);
    }
    
    /**
    * @notice Used to calculate the current upper bound reward.
    * @param period the period to get the upper bound for.
    * @return Returns the current upper bound
    */
    function upperBoundOfPeriod(uint256 period) public pure returns (uint256) {
        return _decayMinuend - (_decaySubtrahend * period);
    }
}