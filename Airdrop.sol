// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

import "./IERC20.sol";
import "./Ownable.sol";

/**
 * @title Contract for airdrops
 */
contract Airdrop is Ownable {
    bool    public hasAllocated;
    uint256 public decimals;
    
    uint256 public numAirdroppers;
    IERC20  internal token;
    uint256 public reward;
    
    mapping(address => bool) public eligableAirdroppers;
    mapping(address => bool) public airdropAdmins;
    
    modifier onlyOwnerOrAdmin() {
        require(msg.sender == owner() || airdropAdmins[msg.sender], "Only the owner or admins may use this function.");
        _;
    }
    
    /**
     * @notice Constructor
     */
    constructor() Ownable(msg.sender) {
        hasAllocated = false;
        decimals = 18;
    }
    
    /**
    * @notice Sets the address of the token.
    * @dev Only callable by the owner or Admin
    * @param addr The address of the token
    */    
    function setDGFAddress(address addr) public onlyOwnerOrAdmin {
        require(addr != address(0), "The address cannot be the zero address");
        token = IERC20(addr);
    }
    
    /**
     * @notice Resets the airdrop.
     * @dev Only callable by the owner or Admin
     */
    function resetAirdrop() public onlyOwnerOrAdmin {
        require(hasAllocated, "Not allocated yet");
        hasAllocated = false;
    }

    /**
     * @notice Used by the owner to add an admin for easy airdrop functionality maintenance.
     * @dev Only callable by the owner
     * @param _admin The address of the new admin
     */
    function addAdmin(address _admin) public onlyOwner {
        require(_admin != address(0), "The address cannot be the zero address");
        require(!airdropAdmins[_admin], "Admin already added");
        airdropAdmins[_admin] = true;
    }
    
    /**
     * @notice Used by the owner to remove an admin for easy airdrop functionality maintenance.
     * @dev Only callable by the owner
     * @param _admin The address of the new admin
     */    
    function revokeAdmin(address _admin) public onlyOwner {
        require(_admin != address(0), "The address cannot be the zero address");
        require(airdropAdmins[_admin], "Admin already revoked");
        airdropAdmins[_admin] = false;
    }
    
    /**
     * @notice Used to set the airdrop amount.
     * @dev Only callable by the owner or admin
     * @param _reward The reward to send to each address
     */    
    function setAirdropReward(uint256 _reward) public onlyOwnerOrAdmin {
        require(_reward > 0, "The reward must be greater than zero");
        reward = _reward;
    }
    
    /**
     * @notice Used to allocate funds to be redeemed at a later time by the calling wallet.
     *
     * @dev Recipients are enabled in chunks of 10k.
     *
     *      Provided the chunk limit, this becomes a two-legged operation (2PC - "Two-Phase-Commit" pattern). 
     *      As a result, the owner/admin is required to call "confirmGlobalAirdropAllocation()" once all recipients are processed.
     *
     *      Rationale:
     *      - Doing so ensures the reward takes all recipients into account.
     *      - Otherwise we would be computing wrong amounts for rewards, since such final reward 
     *        would be updated according to the length of the latest chunk submitted by the owner/admin.
     *
     * @param _recipients The qualified addresses.
     */
    function setAirdropAllocation(address[] memory _recipients) public onlyOwnerOrAdmin {
        require(!hasAllocated, "Already allocated");

        // Only allow for the number of recipients to be added at 10k recipients at a time. 
        uint256 chunkSize = _recipients.length;
        require((chunkSize > 0) && (chunkSize < 10001), "The chunk size must be between 1 and 10000");

        for(uint256 i = 0; i < chunkSize; i++) {
            require(_recipients[i] != address(0), "The recipient address cannot be the zero address");

            // Check for duplicate air droppers in order to spread rewards equally. 
            // Otherwise I could get 99.9% of the rewards just by defining duplicate address (as a malicious admin/owner).
            require(!eligableAirdroppers[_recipients[i]], "Duplicate recipient addresses are not allowed");

            // Enable the address
            eligableAirdroppers[_recipients[i]] = true;
        }

        hasAllocated = true;
    }

    /**
     * @notice Sets the reward based on the total number of recipients.
     *
     * @dev This function exists because "Airdrop.setAirdropAllocation" can be called several times,
     *      and the calculated reward would be limited to 10k recipients only.
     *      This function allows you to set the reward for the total number of recipients.
     *
     * @param totalRecipients The total number of recipients.
     */
    function confirmGlobalAirdropAllocation (uint256 totalRecipients) public onlyOwnerOrAdmin {
        require(hasAllocated, "Airdrop allocation not set");
        require(totalRecipients > 0, "The number of recipients must be greater than zero");

        // Compute the amount for all recipients, instead of 10k recipients only.
        uint256 globalReward = token.balanceOf(address(this)) / totalRecipients;

        // Update the reward taking all recipients into account
        setAirdropReward(globalReward);
    }
    
    /**
    * @notice Used to redeem the airdrop of the message sender.
    */
    function redeemAirdrop() public {
        redeemAirdropForAddress(msg.sender);
    }
    
    /**
    * @notice Used to redeem the airdrop for another user.
    * @param addr The address of the user
    */
    function redeemAirdropFor(address addr) public onlyOwnerOrAdmin {
        redeemAirdropForAddress(addr);
    }

    /**
    * @notice Used to redeem the airdrop for another user.
    * @param addr The address of the user
    */
    function redeemAirdropForAddress(address addr) private {
        require(addr != address(0), "The address cannot be the zero address");
        require(eligableAirdroppers[addr], "There are no tokens left to redeem.");

        uint256 _reward = reward;
        require(_reward > 0, "The reward must be greater than zero");

        require(token.transfer(addr, _reward), "Transfer failed");
        eligableAirdroppers[addr] = false;
    }

    /**
    * @notice Checks the reward of the message sender.
    * @return _reward Returns the reward
    */
    function checkReward() public view returns (uint256 _reward) {
        return checkReward(msg.sender);
    }
    
    /**
    * @notice Checks the reward of the address specified.
    * @param addr The address to check.
    * @return _reward Returns the reward
    */
    function checkReward(address addr) public view returns (uint256 _reward) {
        return (eligableAirdroppers[addr]) ? reward : 0;
    }
}