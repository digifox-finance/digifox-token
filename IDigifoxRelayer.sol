// SPDX-License-Identifier: GPLv3
pragma solidity 0.8.3;

/**
 * @title Relayer interface
 */
interface IDigifoxRelayer {
    function setCurrentPeriod(uint256 _period) external;
    function setPeriodMintedTokens(uint256 _period, uint256 _minted) external;
    function checkAccumulatedRewards(address _check) external view returns (uint256 accumulatedRewards);
    function addToClaimedRewards(address _redeemer, uint256 _minted) external;
    function updateLastTimeRedeemed(address _redeemer, uint256 _period) external;

    function getTotalFeesOfPeriod(uint256 _period) external view returns (uint256 totalReward);
    function checkLastPeriodRedeemed(address _check) external view returns (uint256 lastPeriod);
    function getPointsOfUserByPeriod(address _redeemer, uint256 _period) external view returns (uint256 points);
    function getPointsOfPeriod(uint256 _period) external view returns (uint256 totalPoints);
}